[wantguns.gitlab.io](wantguns.gitlab.io) is live at [wantguns.dev](wantguns.dev).

You can find the old source code at the master branch

Or you can check out a snapshot of the old website using [the wayback machine](https://web.archive.org/web/20200907010627/https://wantguns.gitlab.io/)
